export default {
  signin: {
    content: 'Ir para o login',
    path: 'entrar',
    button: 'Entrar'
  },

  signup: {
    content: 'Criar conta',
    path: 'cadastro',
    button: 'Continuar'
  },

  signup2: {
    path: 'cadastro',
    button: 'Cadastrar'
  },

  home: {
    content: 'Voltar para home',
    path: '/'    
  }
}
