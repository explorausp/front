const commonFields = () => {
  return {
    nusp: {
      type: 'number',
      label: 'NUSP',
      value: ''
    },
    nome: {
      type: 'text',
      label: 'Nome',
      value: ''
    },
    instituto: {
      type: 'text',
      label: 'Instituto',
      value: ''
    },
    cpf: {
      type: 'number',
      label: 'CPF',
      value: ''
    },
    nascimento: {
      type: 'text',
      label: 'Data de nascimento',
      value: ''
    },
    admissao: {
      type: 'text',
      label: 'Data de admissão',
      value: ''
    },
    endLogradouro: {
      type: 'text',
      label: 'Endereço logradouro',
      value: ''
    },
    endNumero: {
      type: 'number',
      label: 'Endereço número',
      value: ''
    },
    endComplemento: {
      type: 'text',
      label: 'Endereço complemento',
      value: ''
    },
    endCEP: {
      type: 'number',
      label: 'CEP',
      value: ''
    }
  }
}

export default {
  signin: {
    email: {
      type: 'text',
      label: 'E-mail',
      value: ''
    },
    password: {
      type: 'password',
      label: 'Senha',
      value: ''
    }
  },

  signup: {
    email: {
      type: 'text',
      label: 'E-mail',
      value: ''
    },
    password: {
      type: 'password',
      label: 'Senha',
      value: ''
    },
    repeatedPassword: {
      type: 'password',
      label: 'Repita a senha',
      value: ''
    }
  },

  signup2: {
    aluno: {
      ...commonFields(),
      curso: {
        type: 'text',
        label: 'Curso',
        value: ''
      }
    },
    professor: {
      ...commonFields(),
      departamento: {
        type: 'text',
        label: 'Departamento',
        value: ''
      },
      sala: {
        type: 'text',
        label: 'Sala',
        value: ''
      }
    },
    administrador: {
      ...commonFields(),
      departamento: {
        type: 'text',
        label: 'Departamento',
        value: ''
      }
    }
  }
}