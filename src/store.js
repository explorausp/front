import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    curriculos: [],
    curriculo: '',
    trilhas: [],
    trilha: '',
    modulos: [],
    modulo: '',
    disciplinas: [],
    disciplina: '',
    status: 'Currículos'
  },

  getters: {
  },

  mutations: {
    curriculos: (state, curriculos) => {
      state.curriculos = curriculos
    },
    curriculo: (state, curriculo) => {
      state.curriculo = curriculo
    },
    trilhas: (state, trilhas) => {
      state.trilhas = trilhas
    },
    trilha: (state, trilha) => {
      state.trilha = trilha
    },
    modulos: (state, modulos) => {
      state.modulos = modulos
    },
    modulo: (state, modulo) => {
      state.modulo = modulo
    },
    disciplinas: (state, disciplinas) => {
      state.disciplinas = disciplinas
    },
    disciplina: (state, disciplina) => {
      state.disciplina = disciplina
    },
    status: (state, status) => {
      state.status = status
    }
  },

  actions: {
    curriculos: async ({ commit }) => {
      await axios
        .get('http://localhost:3000/curriculos/')
        .then(resp => commit('curriculos', resp.data))
    },
    curriculo: async ({ commit }, id) => {
      commit('curriculo', id)

      await axios
        .get(`http://localhost:3000/curriculos/${id}/trilhas`)
        .then(resp => commit('trilhas', resp.data))
        .catch(err => console.log(err))
        .then(() => commit('status', 'Trilhas'))
    },
    trilha: async ({ commit, state }, id) => {
      commit('trilha', id)

      await axios
        .get(`http://localhost:3000/curriculos/${state.curriculo}/trilhas/${id}/modulos`)
        .then(resp => commit('modulos', resp.data))
        .catch(err => console.log(err))
        .then(() => commit('status', 'Módulos'))
    },
    modulo: async ({ commit, state }, id) => {
      commit('modulo', id)

      await axios
        .get(`http://localhost:3000/curriculos/${state.curriculo}/trilhas/${state.trilha}
              /modulos/${id}/disciplinas`)
        .then(resp => commit('disciplinas', resp.data))
        .catch(err => console.log(err))
        .then(() => commit('status', 'Disciplinas'))
    },
    disciplina: async ({ commit }, id) => {
      commit('disciplina', id)

      await axios
        .get(`http://localhost:3000/disciplinas/${id}`)
        .then(resp => commit('disciplina', resp.data))
        .catch(err => console.log(err))
        .then(() => commit('status', 'Disciplina'))
    },
    backtrack: ({ commit, state }) => {
      const status = state.status
      let newStatus = ''

      if (status === 'Disciplina') newStatus = 'Disciplinas'
      if (status === 'Disciplinas') newStatus = 'Módulos'
      else if (status === 'Módulos') newStatus = 'Trilhas'
      else if (status === 'Trilhas') newStatus = 'Currículos'

      commit('status', newStatus)
    }
  }
})
