import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const load = path => import(`@containers/${path}`)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => load('Home')
    },
    {
      path: '/explorar',
      name: 'explore',
      component: () => load('Explore')
    },
    {
      path: '/entrar',
      name: 'signin',
      component: () => load('Login')
    },
    {
      path: '/cadastro',
      name: 'signup',
      component: () => load('Login')
    },
    {
      path: '/cadastro/informacoes-pessoais',
      name: 'signup2',
      component: () => load('Registration')
    },
    {
      path: '/u',
      name: 'user-home',
      component: () => load('UserHome') 
    }
  ]
})

export default router
