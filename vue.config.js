const path = require('path')
const alias = location => path.resolve(__dirname, location)

module.exports = {
  lintOnSave: false,

  chainWebpack: (config) => {
    config.resolve.alias
      .set('@components', alias('src/components'))
      .set('@containers', alias('src/containers'))
      .set('@content', alias('src/content'))
  }
}
